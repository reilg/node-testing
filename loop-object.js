const obj = {
  username: "USERNAME",
  password: "PASSWORD",
  database: "DATABASE_NAME",
  define: {
    dialect: "DIALECT",
    foo: "FOO",
    bar: "BAR",
  }
}

for(const [k, v] of Object.entries(obj)) {
  if (typeof v === 'object') {
    for(const [x, y] of Object.entries(v)) {
      console.log(x, y)
    }
    continue
  }

  console.log(k, v)
}
