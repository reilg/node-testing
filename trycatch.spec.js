const T = require('./trycatch')

test('trycatch is loaded', async () => {
  let t = new T(true)

  try {
    await t.doSomething()
  } catch(e) {
    expect(e).toBe('getThis failed')
  }
})

test('return checks', async () => {
  let t = new T()

  try {
    await t.returnTry()
  } catch(e) {
    expect(e).toBe('NOPE')
  }
})

test('func calls async func without await', () => {
  let t = new T()
  t.noasync()
    .then(o => {
      expect(o).toBe('getThis good')
    })
})

test('error gets caught up in bubble', () => {
  let t = new T()
  expect(t.bubbleError).toThrow(Error)
})
