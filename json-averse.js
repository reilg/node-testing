class MyJSONClass {
  constructor(id, name) {
    this.id = id
    this.name = name
  }

  async setName(name) {
    setTimeout(() => {
      this.name = name
    }, 3000);
  }
}

async function run() {
  const i = new MyJSONClass(1, 'Reil')
  await i.setName('Not Reil')
  console.clear()
  console.log("STRIG", JSON.stringify(i))
}

run()
