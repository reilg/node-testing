const { v4: uid, parse: parse } = require('uuid')
const d64 = require('d64')

module.exports.func = () => {
    var byts = parse(uid())
    return d64.encode(byts)
}
