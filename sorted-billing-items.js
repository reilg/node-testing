const moment = require('moment')

const billingItems = require('./billing-items-list.js')
const transactions = [
  ...billingItems.processed,
  ...billingItems.processing,
]

const txList = []

for (const item of transactions) {
  txList.push({
    id: item.id,
    state: item.state,
    odate: item.orderDate,
    hdate: moment(item.orderDate).format("LLL")
  })
}

const sorted = txList.sort((a, b) => {
  return b.odate.localeCompare(a.odate)
}).slice(0, 10)

// console.log(txList, txList.length)
console.log(sorted, sorted.length)

