const { func: f } = require('./uuid')

const log = console.log

it('should show length of parsed uuid', () => {
  let i = 0
  while(i <= 10) {
    const r = f()
    log(r, r.length)
    i++
  }
})
