export default class A
{
  async foo(a, b)
  {
    return 'something';
  }

  async bar(c, d)
  {
    this.foo(c, d);
  }
}
