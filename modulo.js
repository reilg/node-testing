const x = 1
const y = 10

console.clear()
console.log(`${x} % ${y} = ${x%y}`)

console.log(`10 % 10 = ${10%10}`)
console.log(`10 % 100 = ${10%100}`)
console.log(`100 % 10 = ${100%100}`)
