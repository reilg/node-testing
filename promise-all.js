const { log, clear, sleep } = require('./helpers')

async function t(ms) {
  await sleep(ms)
  return Promise.resolve(`waited ${ms} ms`)
}

async function t2() {
  return [
    Promise.all([
      t(100),
      t(101),
    ]),
    Promise.all([
      t(102),
      t(103)
    ])
  ]
}

async function run() {
  try {

    // const out = await Promise.all([
    //   Promise.all([
    //     t(100),
    //     t(101),
    //   ]),
    //   Promise.all([
    //     t(102),
    //     t(103),
    //   ]),
    //   [
    //     t(104),
    //     t(105),
    //   ],
    //   t(106),
    // ])

    // doesn't work
    // const out = await Promise.all([t2()])
  } catch (err) {
    log('err', err)
  }
}

clear()
run()
