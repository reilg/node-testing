class P {
  job = 'pilot'
  #age = 'secret'

  constructor() {
    this.name = "Han"
  }

  get props() {
    return {
      name: this.name,
      job: this.job,
    }
  }

  async funky() {
    return Promise.resolve("Yes")
  }
}

let p = new P()
console.log("out", p)
console.log("props", p.props)
