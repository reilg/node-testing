const { Child, Base } = require('./class-extend')

global.log = console.log

const data = {
  name: 'Han Solo',
  dateCreated: 'yesterday',
}

it('should construct', () => {
  const c = new Child(data)
  const b = new Base(data)

  log(b)
  log(c)
  expect(c.dateCreated).toBe('yesterday')
})
