function getDiff(a, b) {
  const d1 = new Date(a);
  const d2 = new Date(b);
  const diffTime = Math.abs(d2 - d1);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  return {
    t: `${diffTime} ms`,
    d: `${diffDays} days`,
  }
}

console.clear()
console.log("1", getDiff('01/01/2020', '01/02/2020'))
