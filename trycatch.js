class T {
  constructor(fail) {
    this.fail = fail
  }

  async getThis() {
    var _this = this
    return new Promise((res, rej) => {
      if(_this.fail) {
        return rej("getThis failed")
      }

      return res("getThis good")
    })
  }

  async doThat() {
    var _this = this
    return new Promise((res, rej) => {
      if(_this.fail) {
        return res("doThat good")
      }

      return rej('doThat failed')
    })
  }

  noasync() {
    return this.normal()
  }

  async normal() {
    let get = await this.getThis()
    return get
  }

  async doSomething() {
    try {
      let g = await this.getThis()
      let d = await this.doThat()

      return Promise.resolve({ g: g, d: d })
    } catch(e) {
      return Promise.reject(e)
    }
  }

  async try() {
    try {
      throw("NOPE")
    } catch (e) {
      // return just e does not throw an error
      // return e

      // works
      // throw(e)

      // works
      return Promise.reject(e)
    }
  }

  async returnTry() {
    return await this.try()
  }

  bubbleError() {
    try {
      return this.throwError()
    } catch(err) {
      throw err
    }
  }

  throwError() {
    throw Error('error thrown')
  }
}

module.exports = T
