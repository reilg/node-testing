module.exports = {
  "processed": [
    {
      "type": "billing-items",
      "id": "af21aee6-f896-41bc-99e8-52a711d36d5c",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T19:14:58.668Z",
      "orderDate": "2020-12-03T19:14:58.666Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/af21aee6-f896-41bc-99e8-52a711d36d5c"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "333",
        "amount": "20.00",
        "fee": {
          "amount": "0.05",
          "landlordAbsorbPercent": 1
        },
        "amountTotal": "20.05",
        "createdAt": "2020-12-03T19:14:58.524Z",
        "updatedAt": "2020-12-03T19:15:18.000Z",
        "paymentMethodType": "ACH",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/333"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "6c39ec7d-5226-4bd1-a64c-804dea5c2559",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T18:00:32.763Z",
      "orderDate": "2020-12-03T18:00:32.758Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/6c39ec7d-5226-4bd1-a64c-804dea5c2559"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "332",
        "amount": "20.00",
        "fee": {
          "amount": "1.00",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "21.00",
        "createdAt": "2020-12-03T18:00:32.585Z",
        "updatedAt": "2020-12-03T18:01:09.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/332"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "331f267c-5950-4500-bb29-b231c074c52b",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:55:40.092Z",
      "orderDate": "2020-12-03T17:55:40.090Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/331f267c-5950-4500-bb29-b231c074c52b"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "331",
        "amount": "10.00",
        "fee": {
          "amount": "0.65",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "10.65",
        "createdAt": "2020-12-03T17:55:39.948Z",
        "updatedAt": "2020-12-03T17:56:03.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/331"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "66760623-b818-415a-aa93-cc97a05f95a7",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:28.020Z",
      "orderDate": "2020-12-03T17:30:28.019Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/66760623-b818-415a-aa93-cc97a05f95a7"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "328",
        "amount": "19.00",
        "fee": {
          "amount": "0.97",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "19.97",
        "createdAt": "2020-12-03T17:30:27.944Z",
        "updatedAt": "2020-12-03T17:31:00.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/328"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "5fcdef2a-23b8-4885-b2f4-d3f23ade25fc",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:09.878Z",
      "orderDate": "2020-12-03T17:30:09.876Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/5fcdef2a-23b8-4885-b2f4-d3f23ade25fc"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "326",
        "amount": "14.00",
        "fee": {
          "amount": "0.79",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "14.79",
        "createdAt": "2020-12-03T17:30:09.803Z",
        "updatedAt": "2020-12-03T17:30:30.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/326"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "901edf91-a694-4794-b4d8-37b5f4ae2d00",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:29:50.463Z",
      "orderDate": "2020-12-03T17:29:50.459Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/901edf91-a694-4794-b4d8-37b5f4ae2d00"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "324",
        "amount": "10.00",
        "fee": {
          "amount": "0.65",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "10.65",
        "createdAt": "2020-12-03T17:29:50.197Z",
        "updatedAt": "2020-12-03T17:30:27.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/324"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "84c85570-91c2-4bb4-9cf8-d6402bdf2ff7",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T12:34:41.042Z",
      "orderDate": "2020-12-03T12:34:41.038Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/84c85570-91c2-4bb4-9cf8-d6402bdf2ff7"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "323",
        "amount": "15.00",
        "fee": {
          "amount": "0.83",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "15.83",
        "createdAt": "2020-12-03T12:34:40.923Z",
        "updatedAt": "2020-12-03T12:35:00.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/323"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "f6ca5630-a9b1-4fb1-a9e7-68fb387b9c8f",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T12:32:56.690Z",
      "orderDate": "2020-12-03T12:32:56.686Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/f6ca5630-a9b1-4fb1-a9e7-68fb387b9c8f"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "322",
        "amount": "20.00",
        "fee": {
          "amount": "1.00",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "21.00",
        "createdAt": "2020-12-03T12:32:56.556Z",
        "updatedAt": "2020-12-03T12:33:27.000Z",
        "paymentMethodType": "Card",
        "status": "COMPLETED",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/322"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "6b06818f-0f84-4d62-9c09-eab3645947da",
      "billingItemType": "CHARGE",
      "refType": "BillingCharge",
      "createdAt": "2020-12-03T17:33:29.651Z",
      "orderDate": "2020-12-03T08:00:00.000Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/6b06818f-0f84-4d62-9c09-eab3645947da"
      },
      "billingCharge": {
        "type": "billing-charges",
        "id": "0de6c571-3bfa-43da-9b66-b7ac1488883e",
        "amount": 77,
        "chargeType": "MISC_FEE",
        "createdAt": "2020-12-03T17:33:29.631Z",
        "chargeDate": "2020-12-03T08:00:00.000Z",
        "links": {
          "self": "/billing-charges/0de6c571-3bfa-43da-9b66-b7ac1488883e"
        }
      },
      "payment": null,
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "84ad8e7e-c5b1-4091-89a1-346b7ba91364",
      "billingItemType": "CREDIT",
      "refType": "BillingCharge",
      "createdAt": "2020-12-03T17:31:27.090Z",
      "orderDate": "2020-12-03T08:00:00.000Z",
      "memo": null,
      "state": "PROCESSED",
      "links": {
        "self": "/billing-items/84ad8e7e-c5b1-4091-89a1-346b7ba91364"
      },
      "billingCharge": {
        "type": "billing-charges",
        "id": "69a1024d-ca16-47d6-9c94-0a17079deb18",
        "amount": 155,
        "chargeType": "REFUND",
        "createdAt": "2020-12-03T17:31:27.070Z",
        "chargeDate": "2020-12-03T08:00:00.000Z",
        "links": {
          "self": "/billing-charges/69a1024d-ca16-47d6-9c94-0a17079deb18"
        }
      },
      "payment": null,
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    }
  ],
  "processing": [
    {
      "type": "billing-items",
      "id": "cd6168f7-aa4f-43f1-a641-f9c1383f2017",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:45.276Z",
      "orderDate": "2020-12-03T17:30:45.275Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/cd6168f7-aa4f-43f1-a641-f9c1383f2017"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "330",
        "amount": "62.00",
        "fee": {
          "amount": "2.47",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "64.47",
        "createdAt": "2020-12-03T17:30:45.205Z",
        "updatedAt": "2020-12-03T17:30:47.000Z",
        "paymentMethodType": "Card",
        "status": "PENDING",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/330"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "182d6be2-333c-463f-b217-f47fc04c4c1e",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:35.620Z",
      "orderDate": "2020-12-03T17:30:35.619Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/182d6be2-333c-463f-b217-f47fc04c4c1e"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "329",
        "amount": "16.00",
        "fee": {
          "amount": "0.86",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "16.86",
        "createdAt": "2020-12-03T17:30:35.553Z",
        "updatedAt": "2020-12-03T17:30:38.000Z",
        "paymentMethodType": "Card",
        "status": "PENDING",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/329"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "164c8211-c85e-413e-92d5-8ce34ca80205",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:19.980Z",
      "orderDate": "2020-12-03T17:30:19.978Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/164c8211-c85e-413e-92d5-8ce34ca80205"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "327",
        "amount": "18.00",
        "fee": {
          "amount": "0.93",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "18.93",
        "createdAt": "2020-12-03T17:30:19.864Z",
        "updatedAt": "2020-12-03T17:30:22.000Z",
        "paymentMethodType": "Card",
        "status": "PENDING",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/327"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "38be16b8-7c01-4cc9-96de-5910b8106408",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-12-03T17:30:00.976Z",
      "orderDate": "2020-12-03T17:30:00.974Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/38be16b8-7c01-4cc9-96de-5910b8106408"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "325",
        "amount": "10.00",
        "fee": {
          "amount": "0.65",
          "landlordAbsorbPercent": 0
        },
        "amountTotal": "10.65",
        "createdAt": "2020-12-03T17:30:00.782Z",
        "updatedAt": "2020-12-03T17:30:03.000Z",
        "paymentMethodType": "Card",
        "status": "PENDING",
        "notificationsSent": [
          "LANDLORD_PAYMENT_CREATED",
          "TENANT_PAYMENT_CREATED"
        ],
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/325"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "efc8a7cc-2429-424b-9d13-48712216c652",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-11-23T09:07:25.135Z",
      "orderDate": "2020-11-23T09:07:25.132Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/efc8a7cc-2429-424b-9d13-48712216c652"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "308",
        "amount": "122.61",
        "fee": {
          "amount": "0.31",
          "landlordAbsorbPercent": 100
        },
        "amountTotal": "122.92",
        "createdAt": "2020-11-23T09:07:25.066Z",
        "updatedAt": "2020-11-23T09:07:25.000Z",
        "paymentMethodType": "ACH",
        "status": "SCHEDULED",
        "notificationsSent": null,
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/308"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    },
    {
      "type": "billing-items",
      "id": "dfc63961-65b9-43bc-a746-9642e01eb7eb",
      "billingItemType": "CREDIT",
      "refType": "Payment",
      "createdAt": "2020-11-20T20:42:18.625Z",
      "orderDate": "2020-11-20T20:42:18.621Z",
      "memo": null,
      "state": "PROCESSING",
      "links": {
        "self": "/billing-items/dfc63961-65b9-43bc-a746-9642e01eb7eb"
      },
      "billingCharge": null,
      "payment": {
        "type": "payments",
        "id": "304",
        "amount": "125.61",
        "fee": {
          "amount": "0.32",
          "landlordAbsorbPercent": 100
        },
        "amountTotal": "125.93",
        "createdAt": "2020-11-20T20:42:18.475Z",
        "updatedAt": "2020-11-20T20:42:25.000Z",
        "paymentMethodType": "ACH",
        "status": "SCHEDULED",
        "notificationsSent": null,
        "failedMessage": null,
        "feeAppliedTo": -1,
        "paymentGateway": "Wepay",
        "links": {
          "self": "/payments/304"
        },
        "customerLandlord": null,
        "customerRenter": null,
        "paymentMethod": null,
        "billingAccount": null,
        "property": null,
        "relationshipNames": [
          "customerLandlord",
          "customerRenter",
          "paymentMethod",
          "billingAccount",
          "property"
        ]
      },
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    }
  ],
  "delayed": [
    {
      "type": "billing-items",
      "id": "7b5e081e-8fb4-4a4e-a7fb-29b3694cea8c",
      "billingItemType": "CHARGE",
      "refType": "BillingCharge",
      "createdAt": "2020-11-20T12:55:01.363Z",
      "orderDate": "2020-12-15T12:55:01.331Z",
      "memo": null,
      "state": "DELAYED_PROCESSED",
      "links": {
        "self": "/billing-items/7b5e081e-8fb4-4a4e-a7fb-29b3694cea8c"
      },
      "billingCharge": {
        "type": "billing-charges",
        "id": "65654f07-b4c3-4c18-b4d2-9bf4b103096e",
        "amount": 500,
        "chargeType": "RENT",
        "createdAt": "2020-11-20T12:55:01.349Z",
        "chargeDate": "2020-12-15T12:55:01.000Z",
        "links": {
          "self": "/billing-charges/65654f07-b4c3-4c18-b4d2-9bf4b103096e"
        }
      },
      "payment": null,
      "billingAccount": null,
      "customer": null,
      "property": null,
      "relationshipNames": [
        "billingCharge",
        "payment",
        "billingAccount",
        "customer",
        "property"
      ]
    }
  ],
  "loading": false
}
