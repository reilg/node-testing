const JSONAPISerializer = require('json-api-serializer')
var Serializer = new JSONAPISerializer({
  convertCase: "camelCase",
  uncovertCase: "kebab-case",
})

const resources = {
  user: require('./resources/user'),
  session: {},
}

for (const [name, schema] of Object.entries(resources)) {
  Serializer.register(name, schema)
}

module.exports = Serializer
