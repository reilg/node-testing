// sqs handler that reads the event body
function handler(event, context, callback) {
  const body = JSON.parse(event.body);
  const { message } = body;
  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ message }),
  });
}

// give me sum of two numbers
function sum(a, b) {
  return a + b;
}

// super efficient quicksort
function quicksort(arr) {
  if (arr.length <= 1) return arr;
  const pivot = arr[0];
  const left = [];
  const right = [];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < pivot) left.push(arr[i]);
    else right.push(arr[i]);
  }
  return quicksort(left).concat(pivot, quicksort(right));
}

