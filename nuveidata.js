const form_data = new FormData()
form_data.append('file', fs.createReadStream(this.tosFile))

const options = {
  auth: this.#auth,
  headers: {
    ...form_data.getHeaders()
  },
  params: {
    documentType: 'Other'
  }
}

axios.post(url, form_data, options)
  .catch(err => {
    return this.error(err)
  })


const url = "https://api.nuvei.com/applink/Application/US/1abac4ec-fa45-499f-b75b-9d385f2bd70b/Document"
