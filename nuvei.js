const fs = require('fs')
const axios = require('axios')
const FormData = require('form-data')
const tmp = require('tmp')

const cred = {
  username: "payrentpf",
  password: "cdJPWpT!",
}

const base = "https://api.nuvei.com/applink/Application/US"
const app_id = "840f18ff-2b6f-4cfc-a617-2c982ca8f783"

class N {

  constructor(id) {
    this.app_id = id || app_id
  }

  addDoc() {
    const url = `${base}/${this.app_id}/Document`

    const tmpobj = tmp.fileSync({ prefix: 'nuvei', postfix: '.txt' });
    console.log('File: ', tmpobj.name);
    console.log('FD: ', tmpobj.fd);

    const data = `ip address: 127.0.0.1\n`
      + `datetime: ${new Date()}\n`
      + `approved: yes\n`

    fs.writeFileSync(tmpobj.name, data)

    const fd = new FormData()
    fd.append('file', fs.createReadStream(tmpobj.name))

    // tmpobj.removeCallback()

    const opt = {
      auth: cred,
      headers: {
        ...fd.getHeaders()
      },
      params: {
        documentType: 'Other'
      }
    }

    return axios.post(url, fd, opt)
  }

  getApp() {
    const url = `${base}/${this.app_id}`
    const opt = {
      auth: cred,
    }

    return axios.get(url, opt)
  }

  listDoc() {
    const url = `${base}/${this.app_id}/Documents`
    const opt = {
      auth: cred,
    }

    return axios.get(url, opt)
  }
}

const n = new N()
n.addDoc()
  .then(res => {
    console.log('res', res.status, res.data)
  })
  .catch(e => {
    console.log('err', e.status, e.data)
  })

module.exports = { N: N }
