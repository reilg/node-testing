const { log, clear, sleep } = require('./helpers')

function test() {
  const obj = { message: 'error', data: { some: 'more' } }
  // throw Error(obj)
  throw Error(JSON.stringify(obj))
}

async function run() {
  try {
    test()
  } catch (err) {
    log('err', err.message)
  }
}

clear()
run()
