const OBJ = {
  properties: {
    complianceLevel: 1,
    compliance: {
      acceptanceDate: 'date-string',
    },
  }
}

function get(obj, path) {
  return path.split('.').reduce((o, i) => o[i], obj)
}

console.log('1', get(OBJ, 'properties'))
console.log('2', get(OBJ, 'properties.compliance'))
console.log('2', get(OBJ, ''))
