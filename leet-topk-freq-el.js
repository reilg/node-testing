function topKFrequent(nums, k) {
  let map = {}
  for (n of nums.sort()) {
    n in map ? map[n][1]++ : map[n] = [n, 1]
  }

  return Object.values(map)
    .sort((a, b) => b[1] - a[1])
    .slice(0, k)
    .map(i => i[0])
}

console.clear()
console.log(topKFrequent([1,1,1,2,2,3], 2))
