const moment = require('moment')

// This is a check from jiph codebase
// let formats = [ "yyyy-mm-dd :) HH*mm"]
let formats = [ moment.ISO_8601, "yyyy-mm-dd :) HH*mm"]

function isValid(d) {
  return moment(d, formats, true).isValid();
}

console.log(isValid('2023-02-12'))
